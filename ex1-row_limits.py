import numpy as np
import od_wrapper as od
import math as m
import pylab

#Definitions
angles = np.linspace(0, 2*np.pi, 360, False)
OrigSrc = 500.
OrigDet = 0.

vectors = np.zeros((360, 12), dtype = np.single)
for i in xrange(360):
    a = angles[i]
    vectors[i, 0] = -m.sin(a) * (-OrigSrc)
    vectors[i, 1] = m.cos(a) * (-OrigSrc)
    vectors[i, 2] = 0
    vectors[i, 3] = -m.sin(a) * OrigDet
    vectors[i, 4] = m.cos(a) * OrigDet
    vectors[i, 5] = 0
    vectors[i, 6] = m.cos(a) * 1.0
    vectors[i, 7] = m.sin(a) * 1.0
    vectors[i, 8] = 0
    vectors[i, 9] = 0
    vectors[i, 10] = 0
    vectors[i, 11] = 1.0

vol_width = 128
det_width = 192
det_height = vol_width

begins = []
ends = []
prov = od.ProjectionDataProvider()
prov.setROI(0., 0., 128, 128)
prov.setVectors(vectors)

for i in xrange(360):
    res = prov.getRowLimits(i)
    print i, res, 0.5 * (res[0] + res[1])
    begins.append(res[0])
    ends.append(res[1])

pylab.gray()
pylab.figure(1)
pylab.plot(begins, angles * 180 / np.pi)
pylab.plot(ends, angles * 180 / np.pi)

line = np.linspace(-75., 75., 100, False)

pylab.xlabel("Coordinate along width of the detector")
pylab.ylabel("Angle")
pylab.show()
