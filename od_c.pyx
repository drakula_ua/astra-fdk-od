# distutils: language = c++
# distutils: sources = od.cpp

from libcpp.pair cimport pair

import cython
import math as m
import numpy as np
cimport numpy as np

cdef extern from "od.h":
  ctypedef pair[float, float] float_pair
  cpdef cppclass DataProvider:
    DataProvider() except +
    void setOriginal(float* arr);
    void setVectors(float* vec);
    void setAngleTable(double* arr, int num)
    void setAngles(int val)
    void setWidth(int val);
    void setHeight(int val);
    void setROI(float xcent, float ycent, int sizex, int sizey);

    int getWidth();
    int getAngles();
    int getHeight();
    int getMaxRowSize();
    float getCent_x();
    float getCent_y();
    float getRowCenter(int i);
    float getAngle(int i);

    float_pair getRowLimits(int i);
    void calcBounds();
    void getPart(float* res);

    float* linearDecimate(float original[], int size, int dec_ord);
    float* linearDecimateBy2(float original[], int size);
    void decimatePart(float* part, int dec_ord);

    void convolution(float* inp);

cdef class ProjectionDataProvider:
  cdef DataProvider* ptr;

  def __cinit__(self):
    self.ptr = new DataProvider()
  def __dealloc__(self):
    del self.ptr

  def setAngleTable(self, np.ndarray[double, ndim=1, mode="c"] input not None):
    self.ptr.setAngleTable(&input[0], input.shape[0])
  def setOriginal(self, np.ndarray[float, ndim=3, mode="c"] input not None):
    self.ptr.setHeight(input.shape[0])
    self.ptr.setAngles(input.shape[1])
    self.ptr.setWidth(input.shape[2])
    self.ptr.setOriginal(&input[0, 0, 0])
  def setVectors(self, np.ndarray[float, ndim=2, mode="c"] input not None):
    self.ptr.setVectors(&input[0, 0])
  def setAngles(self, val):
    self.ptr.setAngles(val)
  def setWidth(self, val):
    self.ptr.setWidth(val)
  def setHeight(self, val):
    self.ptr.setHeight(val)
  def setROI(self, cent_x, cent_y, xsize, ysize):
    self.ptr.setROI(cent_x, cent_y, xsize, ysize)

  def getMaxRowSize(self):
    return self.ptr.getMaxRowSize()
  def getRowLimits(self, i):
    return self.ptr.getRowLimits(i)
  def calcBounds(self):
    self.ptr.calcBounds()
  def getPart(self):
    #===========1-Calculate limits for every row and max_row_size
    self.ptr.calcBounds()
    #===========2-Allocate numpy array of required size
    cdef np.ndarray[float, ndim = 3] part
    part_width = self.ptr.getMaxRowSize()
    #part_width = self.ptr.getWidth()
    part_angles = self.ptr.getAngles()
    part_height = self.ptr.getHeight()
    part = np.zeros((part_height, part_angles, part_width), dtype = np.single)
    #===========3-Push data
    self.ptr.getPart(&part[0,0,0])
    return part

  def getGeomVect(self, original_vect, OrigSrc, OrigDet, dec_ord):
    dec_power = 2**dec_ord
    vect = np.copy(original_vect[::dec_power])
    cent_x = self.ptr.getCent_x()
    cent_y = self.ptr.getCent_y()
    '''
    Positions relatively to the center of ROI (angle = 0) (No)
    DetCent : (-cent_x + shift, OrigDet - cent_y, 0)
    RayCent : (-cent_x, -OrigSrc - cent_y, 0)
    '''
    for i in xrange(vect.shape[0]):
      a = self.ptr.getAngle(i * dec_power)
      vect[i, 0] = - m.sin(a) * (-OrigSrc) - cent_x
      vect[i, 1] = + m.cos(a) * (-OrigSrc) - cent_y
      vect[i, 2] = 0
      shift = self.ptr.getRowCenter(i * dec_power)
      vect[i, 3] = m.cos(a) * shift - m.sin(a) * (OrigDet) - cent_x
      vect[i, 4] = m.sin(a) * shift + m.cos(a) * (OrigDet) - cent_y
      vect[i, 5] = 0

    return vect

  def decimatePart(self, np.ndarray[float, ndim=3, mode="c"] part not None, dec_ord):
    self.ptr.decimatePart(&part[0, 0, 0], dec_ord)
    size = part.shape[1]
    for i in xrange(dec_ord):
      size /= 2
    return part[:,0:size,:]
  def getDecimatedVectors(self, np.ndarray[float, ndim=2, mode="c"] vec not None, dec_ord):
    return vec[::2**dec_ord,:]

  def convolution(self, np.ndarray[float, ndim=3, mode="c"] input not None):
    self.ptr.convolution(&input[0,0,0])

def calcROIprops(size_x, size_y, s, i):
  # res = [cent_x, cent_y, off_x, off_y, size_x, size_y]
  res = [0., 0., 0., 0., size_x, size_y]
  X = size_x
  Y = size_y
  quot = i
  rem = 0

  for l in xrange(s):
    delta_x = [X/4., -X/4., X/4., -X/4.]
    delta_y = [-Y/4., -Y/4., Y/4., Y/4.]
    add_off_x = [0, 0, X/2, X/2]
    add_off_y = [Y/2, 0, X/2, 0]
    sizes_x = [X - X/2, X/2, X - X/2, X/2]
    sizes_y = [Y/2, Y/2, Y - Y/2, Y - Y/2]

    rem = quot % 4
    quot /= 4

    res[0] += delta_x[rem];
    res[1] += delta_y[rem];
    res[2] += add_off_x[rem];
    res[3] += add_off_y[rem];
    res[4] = sizes_x[rem];
    res[5] = sizes_y[rem];
    X = res[4]
    Y = res[5]

  return res
