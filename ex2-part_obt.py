import astra
import numpy as np
import od_wrapper as od
import math as m
import pylab

#Definitions
num_angles = 360
angles = np.linspace(0, 2*np.pi, num_angles, False)
OrigSrc = 500
OrigDet = 0
vectors = np.zeros((num_angles, 12), dtype = np.single)
for i in xrange(num_angles):
    a = angles[i]
    vectors[i, 0] = -m.sin(a) * (-OrigSrc)
    vectors[i, 1] = m.cos(a) * (-OrigSrc)
    vectors[i, 2] = 0
    vectors[i, 3] = -m.sin(a) * OrigDet
    vectors[i, 4] = m.cos(a) * OrigDet
    vectors[i, 5] = 0
    vectors[i, 6] = m.cos(a) * 1.0
    vectors[i, 7] = m.sin(a) * 1.0
    vectors[i, 8] = 0
    vectors[i, 9] = 0
    vectors[i, 10] = 0
    vectors[i, 11] = 1.0

vol_width = 128
det_width = 192
det_height = vol_width
sod = 500
sdd = 500

vol_geom = astra.create_vol_geom(vol_width, vol_width, vol_width)
proj_geom = astra.create_proj_geom('cone', 1.0, 1.0, det_height, det_width, angles, sod, sdd - sod)

cube = np.zeros((128,128,128))
cube[17:113,17:113,17:113] = 1
cube[33:97,33:97,33:97] = 0
proj_id, proj_data = astra.create_sino3d_gpu(cube, proj_geom, vol_geom)

provider = od.ProjectionDataProvider()
provider.setOriginal(proj_data)
provider.setAngleTable(angles)
provider.setVectors(vectors)
provider.setROI(32., 32., 64, 64)

provider.calcBounds()
print provider.getMaxRowSize()
part = provider.getPart()
print "raw part -", part.shape
part = provider.decimatePart(part, 1)
print "decimated part -", part.shape

pylab.gray()
pylab.figure(1)
pylab.imshow(proj_data[50,:,:])
pylab.figure(2)
pylab.imshow(part[50,:,:])
pylab.show()
