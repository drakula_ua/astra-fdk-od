#include "utility"
using namespace std;

typedef pair<float, float> float_pair;

class DataProvider {
private:
  float* original;
  float* geom_vectors;
  double* angle_table;
  int angles;
  int width;
  int height;

  float cent_x;
  float cent_y;
  int xsize;
  int ysize;

  int* start_row;
  int* end_row;
  int* size_row;
  float* row_center;
  int max_row_size;
public:
  DataProvider() {
    start_row = NULL;
    end_row = NULL;
    size_row = NULL;
    row_center = NULL;
  }
  ~DataProvider() {
    if(start_row != NULL) delete[] start_row;
    if(end_row != NULL) delete[] end_row;
    if(size_row != NULL) delete[] size_row;
    if(row_center != NULL) delete[] row_center;
  }

  void setOriginal(float* arr) { original = arr; }
  void setVectors(float* vec) { geom_vectors = vec; }
  void setAngles(int val) { angles = val; }
  void setWidth(int val) { width = val; }
  void setHeight(int val) { height = val; }
  void setAngleTable(double* arr, int num) {
    angle_table = arr;
    angles = num;
  }
  void setROI(float xcent, float ycent, int sizex, int sizey) {
    cent_x = xcent;
    cent_y = ycent;
    xsize = sizex;
    ysize = sizey;
  }

  int getWidth() { return width; }
  int getAngles() { return angles; }
  int getHeight() { return height; }
  float getCent_x() { return cent_x; }
  float getCent_y() { return cent_y; }
  int getMaxRowSize() { return max_row_size; }
  float getAngle(int i) { return angle_table[i]; }
  float getRowCenter(int i) {
    return row_center[i];
  }

  float_pair getRowLimits(int i);
  void calcBounds();
  void getPart_rough(float* res);
  void getPart_fine(float* res);
  void getPart(float* res) { getPart_fine(res); }

  float* linearDecimate(float original[], int size, int dec_ord);
  float* linearDecimateBy2(float original[], int size);
  void decimatePart(float* part, int dec_ord);

  void convolution(float* inp);
};
