import astra
import numpy as np
import math as m
import od_wrapper as od
import pylab

num_angles = 360
angles = np.linspace(0, 2*np.pi, num_angles, False)

cube = np.zeros((128,128,128))
cube[17:113,17:113,17:113] = 1
cube[33:97,33:97,33:97] = 0

OrigSrc = 500
OrigDet = 0

vectors = np.zeros((num_angles, 12), dtype = np.single)
for i in xrange(num_angles):
    a = angles[i]
    vectors[i, 0] = -m.sin(a) * (-OrigSrc)
    vectors[i, 1] = m.cos(a) * (-OrigSrc)
    vectors[i, 2] = 0
    vectors[i, 3] = -m.sin(a) * OrigDet
    vectors[i, 4] = m.cos(a) * OrigDet
    vectors[i, 5] = 0
    vectors[i, 6] = m.cos(a) * 1.0
    vectors[i, 7] = m.sin(a) * 1.0
    vectors[i, 8] = 0
    vectors[i, 9] = 0
    vectors[i, 10] = 0
    vectors[i, 11] = 1.0

vol_geom = astra.create_vol_geom(128, 128, 128)
sin_geom = astra.create_proj_geom('cone_vec', 128, 192, vectors)
sin_id, sin_data = astra.create_sino3d_gpu(cube, sin_geom, vol_geom)

provider = od.ProjectionDataProvider()
print sin_data.shape
provider.setOriginal(sin_data)
provider.setAngleTable(angles)
provider.setVectors(vectors)

provider.convolution(sin_data)
astra.data3d.store(sin_id, sin_data)

cent_x = 48.
cent_y = 48.
part_w = 32
part_h = 32

provider.setROI(cent_x, cent_y, part_w, part_h)
part = provider.getPart()

vol_geom = astra.create_vol_geom(32, 32, 128)
rec_id = astra.data3d.create('-vol', vol_geom)
part_vec = provider.getGeomVect(vectors, cent_x, cent_y, OrigSrc, OrigDet)

#decimation
dec_stages = 0
part = provider.decimatePart(part, dec_stages)
part_vec = provider.getDecimatedVectors(part_vec, dec_stages)
print part.shape

part_geom = astra.create_proj_geom('cone_vec', 128, provider.getMaxRowSize(), part_vec)
part_id = astra.data3d.create('-sino', part_geom, part)

cfg = astra.astra_dict('BP3D_CUDA')
cfg['ReconstructionDataId'] = rec_id
cfg['ProjectionDataId'] = part_id

alg_id = astra.algorithm.create(cfg)

astra.algorithm.run(alg_id)

rec = astra.data3d.get(rec_id)

pylab.figure(1)
pylab.imshow(part[65,:,:])
pylab.figure(2)
pylab.imshow(sin_data[65,:,:])
pylab.figure(3)
pylab.imshow(rec[48,:,])
#pylab.clim(-0.05, 1.05)
pylab.show()

astra.algorithm.delete(alg_id)
astra.data3d.delete(rec_id)
astra.data3d.delete(sin_id)
astra.data3d.delete(part_id)
