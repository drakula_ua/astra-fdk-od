from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

setup(ext_modules =
      [Extension("od_wrapper", ["od_c.pyx", "od.cpp"], language="c++",)],
      cmdclass={"build_ext": build_ext})
