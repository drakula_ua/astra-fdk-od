import astra
import numpy as np
import math as m
import od_wrapper as od
import pylab

#Geometry init
num_angles = 360
angles = np.linspace(0, 2*np.pi, num_angles, False)
OrigSrc = 500
OrigDet = 0
#Setting values in main geometry vector according to the documentation
vectors = np.zeros((num_angles, 12), dtype = np.single)
for i in xrange(num_angles):
    a = angles[i]
    vectors[i, 0] = -m.sin(a) * (-OrigSrc)
    vectors[i, 1] = m.cos(a) * (-OrigSrc)
    vectors[i, 2] = 0
    vectors[i, 3] = -m.sin(a) * OrigDet
    vectors[i, 4] = m.cos(a) * OrigDet
    vectors[i, 5] = 0
    vectors[i, 6] = m.cos(a) * 1.0
    vectors[i, 7] = m.sin(a) * 1.0
    vectors[i, 8] = 0
    vectors[i, 9] = 0
    vectors[i, 10] = 0
    vectors[i, 11] = 1.0
#Phantom = hollow cube
cube = np.zeros((128,128,128))
cube[17:113,17:113,17:113] = 1
cube[33:97,33:97,33:97] = 0
#Creating sinograms with astra functions
vol_geom = astra.create_vol_geom(128, 128, 128)
sin_geom = astra.create_proj_geom('cone_vec', 128, 192, vectors)
sin_id, sin_data = astra.create_sino3d_gpu(cube, sin_geom, vol_geom)
#Init of the FDK-OD data provider
provider = od.ProjectionDataProvider()
provider.setOriginal(sin_data)        #original sinogram data
provider.setAngleTable(angles)        #values of angles
provider.setVectors(vectors)          #geometry vectors
#Manual convolution with ramp filter
provider.convolution(sin_data)
astra.data3d.store(sin_id, sin_data)
#setting number of decomposition stages
s = 3
dec_stages = 3
parts_num = 4**s
#empty array for the result
res_vol = np.zeros((128, 128, 128))
#iterating over parts of the volume
for i in xrange(parts_num):
    #get its characteristics
    [cent_x, cent_y, off_x, off_y, size_x, size_y] = od.calcROIprops(128, 128, s, i)
    #set these properties in data provider
    provider.setROI(cent_x, cent_y, size_x + 2, size_y + 2)
    #get the sinogram data required for the reconstruction of this part
    part = provider.getPart()
    #decimate sinogram data and update vectors
    part = provider.decimatePart(part, dec_stages)
    #edit geometry vectors to take row shifts and center of part into consideration
    part_vec = provider.getGeomVect(vectors, OrigSrc, OrigDet, dec_stages)
    #astra config
    vol_geom = astra.create_vol_geom(size_x, size_y, 128)
    rec_id = astra.data3d.create('-vol', vol_geom)
    part_geom = astra.create_proj_geom('cone_vec', 128, provider.getMaxRowSize(), part_vec)
    part_id = astra.data3d.create('-sino', part_geom, part)
    #more astra config
    cfg = astra.astra_dict('BP3D_CUDA')
    cfg['ReconstructionDataId'] = rec_id
    cfg['ProjectionDataId'] = part_id
    alg_id = astra.algorithm.create(cfg)
    astra.algorithm.run(alg_id)
    #get the reconstructed part
    rec = astra.data3d.get(rec_id)
    #and insert it into bigger volume
    res_vol[:,off_x:off_x+size_x,off_y:off_y+size_y] = rec
    #clean
    astra.algorithm.delete(alg_id)
    astra.data3d.delete(rec_id)
    astra.data3d.delete(part_id)

#phantom reconstructed without decomposition
ref = np.load("refArr.npy")
#comparison
pylab.figure(1)
pylab.imshow(2**dec_stages*res_vol[48,:,])
pylab.clim((0, 1.2*10**3))
pylab.figure(2)
pylab.imshow(2**dec_stages*res_vol[48,:,] - ref[48,:,:])

print np.mean(2**dec_stages*res_vol[48,:,] - ref[48,:,:])
print np.std(2**dec_stages*res_vol[48,:,] - ref[48,:,:])

pylab.show()

astra.data3d.delete(sin_id)
