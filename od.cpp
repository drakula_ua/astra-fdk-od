#include "math.h"
#include "stdio.h"
#include "string.h"
#include "fftw3.h"
#include <algorithm>

#include "od.h"

float_pair DataProvider::getRowLimits(int i)
{
  bool change = true;
  float begin = 0;
  float end = 0;
  float xs[4] = {cent_x - xsize/2, cent_x - xsize/2, cent_x + xsize/2, cent_x + xsize/2};
  float ys[4] = {cent_y - ysize/2, cent_y + ysize/2, cent_y - ysize/2, cent_y + ysize/2};

  float Cuw, Cux, Cuy, Cdw, Cdx, Cdy;
  float fSrcX, fSrcY, fDetSX, fDetSY, fDetUX, fDetUY, fDetVZ;
  float fUNum, fDen, proj_a;

  for(int k = 0; k < 4; k++) {

    /* used before
    float U = R + ys[i] * cos(a) + xs[i] * sin(a);
    float proj_a = R * (xs[i] * cos(a) - ys[i] * sin(a)) / U;
    */

    //detector should be parallel to z-axis. At least for now. So DetVX = DetVY = 0.

    fSrcX = geom_vectors[12*i + 0];
    fSrcY = geom_vectors[12*i + 1];
    fDetSX = geom_vectors[12*i + 3];
    fDetSY = geom_vectors[12*i + 4];
    fDetUX = geom_vectors[12*i + 6];
    fDetUY = geom_vectors[12*i + 7];
    fDetVZ = geom_vectors[12*i + 11];

    //printf("%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n", fSrcX, fSrcY, fSrcZ, fDetSX, fDetSY, fDetSZ, fDetUX, fDetUY, fDetUZ, fDetVX, fDetVY, fDetVZ);

    /* Original equations. Delete parts with VY and VX
    Cuw = (fDetSY*fDetVZ - fDetSZ*fDetVY)*fSrcX - (fDetSX*fDetVZ - fDetSZ*fDetVX)*fSrcY + (fDetSX*fDetVY - fDetSY*fDetVX)*fSrcZ;
    Cux = (fDetSZ - fSrcZ)*fDetVY - (fDetSY - fSrcY)*fDetVZ;
    Cuy = (fDetSX - fSrcX)*fDetVZ - (fDetSZ - fSrcZ)*fDetVX;

    Cdw = -fSrcX * (fDetUY*fDetVZ - fDetUZ*fDetVY) - fSrcY * (fDetUZ*fDetVX - fDetUX*fDetVZ) - fSrcZ * (fDetUX*fDetVY - fDetUY*fDetVX);
    Cdx = fDetUY*fDetVZ - fDetUZ*fDetVY;
    Cdy = fDetUZ*fDetVX - fDetUX*fDetVZ;
    */

    Cuw = fDetSY*fDetVZ*fSrcX - fDetSX*fDetVZ*fSrcY;
    Cux = -(fDetSY - fSrcY)*fDetVZ;
    Cuy = (fDetSX - fSrcX)*fDetVZ;

    Cdw = -fSrcX*fDetUY*fDetVZ + fSrcY*fDetUX*fDetVZ;
    Cdx = fDetUY*fDetVZ;
    Cdy = -fDetUX*fDetVZ;

    fUNum = Cuw + xs[k] * Cux + ys[k] * Cuy;
    fDen = Cdw + xs[k] * Cdx + ys[k] * Cdy;
    proj_a = fUNum / fDen;

    if((proj_a < begin) || change) {
      begin = proj_a;
    }
    if((proj_a > end) || change) {
      end = proj_a;
    }
    change = false;
  }
  return float_pair(begin, end);
}

void DataProvider::calcBounds()
{
  start_row = new int[angles];
  end_row = new int[angles];
  size_row = new int[angles];
  row_center = new float[angles];
  max_row_size = 0;

  int tmp_start = 0;
  int tmp_end = 0;
  int dif = 0;
  for(int i = 0; i < angles; i++) {
    float_pair tmp_lim = getRowLimits(i);
    row_center[i] = 0.5f * (tmp_lim.first + tmp_lim.second);

    tmp_start = static_cast<int>(tmp_lim.first) + width/2 - 1;
    //checking getting over left and right border
    tmp_start = max(0, tmp_start);
    tmp_start = min(width - 1, tmp_start);

    tmp_end = static_cast<int>(tmp_lim.second) + width/2 + 1;
    tmp_end = max(0, tmp_end);
    tmp_end = min(width - 1, tmp_end);

    dif = tmp_end - tmp_start;
    /*
    Size of the row is either odd or even. For storage and decimation
    we need to center all rows. For this sake we will make all rows contain
    odd number of pixels
    */

    if(dif % 2 == 1) {
      if((tmp_end < width - 2) && (tmp_end > 0)) {
        tmp_end++;
        row_center[i] += 0.5f;
      } else {
        if((tmp_start > 1) && (tmp_start < width - 1)) {
          tmp_start--;
          row_center[i] -= 0.5f;
        }
      }
    }

    //=============================END=====================================

    start_row[i] = tmp_start;
    end_row[i] = tmp_end;
    dif = end_row[i] - start_row[i];
    size_row[i] = dif;
    if (dif > max_row_size) {
      max_row_size = dif;
    }
    //row_center[i] = 0.5f * (start_row[i] + end_row[i] - width);

  }
}

void DataProvider::getPart_rough(float* res)
{
  int part_width = max_row_size;
  int part_area = max_row_size * angles;
  int original_area = width * angles;

  int start = 0;
  int dif = 0;
  int shift = 0;

  for(int i = 0; i < angles; i++) {
    start = start_row[i];
    shift = (part_width - size_row[i]) / 2;
    /* There are several ideas how to store data properly and decimate it.
    We can store only the information inside of the row limits.
    Shift like this
    000-------000
    0-----------0
    0000-----0000

    for(int h = 0; h < height; h++) {
      memcpy(&(res[h*res_area + i*res_width + shift]), &(original[h*original_area + i*width + start]), dif * sizeof(float));
    }
    */

    /* On the other hand, in every row with fewer elements than max size we can
    store spare values and use them for decimation
    ***-------***
    *-----------*
    ****-----****
    */
    for(int h = 0; h < height; h++) {
      memcpy(&(res[h*part_area + i*part_width]), &(original[h*original_area + i*width + start - shift]), part_width * sizeof(float));
    }

  }
}

void DataProvider::getPart_fine(float* res)
{
  int part_width = max_row_size;
  int part_area = max_row_size * angles;
  int original_area = width * angles;

  int start = 0;
  int dif = 0;
  int shift = 0;
  float fract;
  float* temp;
  /*
  for(int i = 0; i < angles; i++) {
    start = start_row[i];
    shift = (part_width - size_row[i]) / 2;
    for(int h = 0; h < height; h++) {
      memcpy(&(res[h*part_area + i*part_width]), &(original[h*original_area + i*width + start - shift]), part_width * sizeof(float));
    }
  }
  */

  for(int i = 0; i < angles; i++) {
    start = start_row[i];
    dif = size_row[i];
    shift = (part_width - dif) / 2;
    temp = new float[dif + 2];

    fract = +( row_center[i] - 0.5f * (start_row[i] + end_row[i] - width) );
    //fract = 0.0f;

    for(int h = 0; h < height; h++) {

      memcpy(&(temp[1]), &(original[h*original_area + i*width + start]), dif * sizeof(float));
      temp[0] = temp[dif + 1] = 0;
      /*
      if((h == 50) && (i == 45)) {
        for(int a = 0; a < dif + 2; a++) {
          printf("%lf ", temp[a]);
        }
        printf("\n");
      }
      */
      if(fract >= 0) {
        for(int x = 0; x < dif; x++) {
          res[h*part_area + i*part_width + shift + x] = (1 - fract) * temp[x + 1] + fract * temp[x + 2];
        }
      }
      if(fract < 0) {
        for(int x = 0; x < dif; x++) {
          res[h*part_area + i*part_width + shift + x] = (1 + fract) * temp[x + 1] - fract * temp[x];
        }
      }
      /*
      if((h == 50) && (i == 45)) {
        for(int a = 0; a < dif; a++) {
          printf("%lf ", res[h*part_area + i*part_width + shift + a]);
        }
        printf("\n");
      }
      */
    }


    delete[] temp;
  }
}

float* DataProvider::linearDecimateBy2(float original[], int size)
{
  const float filter[3] = {0.25f, 0.5f, 0.25f};
  int halfsize = size >> 1;
  float* res = new float[halfsize];

  res[0] = filter[0] * original[size - 1] +
           filter[1] * original[0] +
           filter[2] * original[1];

  for(int i = 1; i < halfsize; i++) {
    int ii = i << 1;
    res[i] = filter[0] * original[ii - 1] +
             filter[1] * original[ii] +
             filter[2] * original[ii + 1];
  }

  return res;
}

float* DataProvider::linearDecimate(float original[], int size, int dec_ord)
{
  if(dec_ord > 0) {
    int current_size = size;
    float* res = linearDecimateBy2(original, current_size);
    current_size /= 2;

    for(int i = 1; i < dec_ord; i++) {
      float* st = linearDecimateBy2(res, current_size);
      delete[] res;
      res = st;
      current_size /= 2;
    }

    return res;
  } else {
    float* res = new float[size];
    memcpy(res, original, size * sizeof(float));
    return res;
  }
}

void DataProvider::decimatePart(float* part, int dec_ord)
{
  int part_width = max_row_size;
  int part_area = max_row_size * angles;

  int dec_size = angles;
  int dec_power = 1;
  for(int i = 0; i < dec_ord; i++) {
    dec_size /= 2;
    dec_power *= 2;
  }

  float* originalColumn = new float[angles];
  float* filteredColumn;

  float* sin_data = new float[part_area];
  float* originalRow = new float[part_width];
  float* stretchedRow = new float[part_width];
  int shift;

  //for every sinogram
  for(int k = 0; k < height; k++) {
    //decimation
    for(int i = 0; i < part_width; i++) {

      for(int a = 0; a < angles; a++) {
        originalColumn[a] = part[k*part_area + a*part_width + i];
      }
      filteredColumn = linearDecimate(originalColumn, angles, dec_ord);
      for(int a = 0; a < dec_size; a++) {
        part[k*part_area + a*part_width + i] = filteredColumn[a];
      }
      delete[] filteredColumn;

    }
  }

  for(int i = 0; i < angles; i++) {
    //row_center[i] = 0.5f * (start_row[i] + end_row[i] - width) - 0.0f;
  }

  delete[] originalColumn;
  delete[] sin_data;
  delete[] originalRow;
  delete[] stretchedRow;
}

void DataProvider::convolution(float* inp)
{
  int zs = width;
  int ys = angles;
  int xs = height;

  fftw_complex *fft_filt, *data, *fft_data, *fft_convol, *convol;
  fftw_plan plan_data, plan_res;
  fft_filt = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * (2*zs));
  data = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * (2*zs));
  fft_data = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * (2*zs));
  fft_convol = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * (2*zs));
  convol = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * (2*zs));

  printf("Convolution\n");

  double scale = 1.0 / (2*zs);
  //ATTENTION
  double pixel_size = 1.0;
  double theta = 0.;
  double sod = 500.;
  double sdd = 500.;
  double pixel_scaling = 10.0 / ((pixel_size * sod / sdd) * cos(theta * M_PI / 180.0));

  for(int i = 0; i < 2*zs; i++) {
    fft_filt[i][0] = (double)(zs - abs(zs - i)) / (2*zs);
    //ATTENTION
    fft_filt[i][0] *= pixel_scaling;

    fft_filt[i][1] = 0.;
  }
  fft_filt[0][0] = 1.0 / 4.0;
  for(int i = 1; i < zs; i += 2) {
    fft_filt[0][0] -= 2.0 / ((M_PI * i) * (M_PI * i));
  }
  //ATTENTION
  fft_filt[0][0] *= pixel_scaling;

  plan_data = fftw_plan_dft_1d(2*zs, data, fft_data, FFTW_FORWARD, FFTW_MEASURE);
  plan_res = fftw_plan_dft_1d(2*zs, fft_convol, convol, FFTW_BACKWARD, FFTW_MEASURE);
  for(int i = 0; i < xs; i++) {
    for(int k = 0; k < ys; k++) {
      for(int j = 0; j < 2*zs; j++) {
          data[j][0] = 0.;
          data[j][1] = 0.;
      }
      for(int j = zs/2; j < 3*zs/2; j++) {
          //float factor_part = R*R + (k - ys/2)*(k - ys/2) + (j - zs/2)*(j - zs/2);
          //float factor = R / sqrt(factor_part);

          data[j][0] = inp[i * ys*zs + k * zs + (j - zs/2)];
          data[j][1] = 0.;
      }
      fftw_execute(plan_data);

      for(int j = 0; j < 2*zs; j++) {
          fft_convol[j][0] = (fft_filt[j][0] * fft_data[j][0] -
                             fft_filt[j][1] * fft_data[j][1]) * scale;
          fft_convol[j][1] = (fft_filt[j][0] * fft_data[j][1] +
                             fft_filt[j][1] * fft_data[j][0]) * scale;
      }
      fftw_execute(plan_res);

      for(int j = 0; j < zs; j++) {
        inp[i * ys*zs + k * zs + j] = convol[j + zs/2][0];
      }
    }
  }

  fftw_destroy_plan(plan_data);
  fftw_destroy_plan(plan_res);
  fftw_free(fft_filt);
  fftw_free(data);
  fftw_free(fft_data);
  fftw_free(fft_convol);
  fftw_free(convol);
}
