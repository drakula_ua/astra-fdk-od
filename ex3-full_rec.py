import astra
import numpy as np
import math as m
import od_wrapper as od
import pylab

num_angles = 360
angles = np.linspace(0, 2*np.pi, num_angles, False)

cube = np.zeros((128,128,128))
cube[17:113,17:113,17:113] = 1
cube[33:97,33:97,33:97] = 0

OrigSrc = 500
OrigDet = 0

vectors = np.zeros((num_angles, 12))
for i in xrange(num_angles):
    a = angles[i]
    vectors[i, 0] = -m.sin(a) * (-OrigSrc)
    vectors[i, 1] = m.cos(a) * (-OrigSrc)
    vectors[i, 2] = 0
    vectors[i, 3] = -m.sin(a) * OrigDet
    vectors[i, 4] = m.cos(a) * OrigDet
    vectors[i, 5] = 0
    vectors[i, 6] = m.cos(a) * 1.0
    vectors[i, 7] = m.sin(a) * 1.0
    vectors[i, 8] = 0
    vectors[i, 9] = 0
    vectors[i, 10] = 0
    vectors[i, 11] = 1.0

proj_geom = astra.create_proj_geom('cone_vec', 128, 192, vectors)
vol_geom = astra.create_vol_geom(128, 128, 128)

proj_id, proj_data = astra.create_sino3d_gpu(cube, proj_geom, vol_geom)
rec_id = astra.data3d.create('-vol', vol_geom)

cfg = astra.astra_dict('FDK_CUDA')
cfg['ReconstructionDataId'] = rec_id
cfg['ProjectionDataId'] = proj_id

alg_id = astra.algorithm.create(cfg)

astra.algorithm.run(alg_id)

provider = od.ProjectionDataProvider()
provider.setOriginal(proj_data)
provider.setAngleTable(angles)

# BP3D + our convolution
provider.convolution(proj_data)
astra.data3d.store(proj_id, proj_data)
rec_id2 = astra.data3d.create('-vol', vol_geom)

cfg = astra.astra_dict('BP3D_CUDA')
cfg['ReconstructionDataId'] = rec_id2
cfg['ProjectionDataId'] = proj_id
alg_id = astra.algorithm.create(cfg)
astra.algorithm.run(alg_id)

rec = astra.data3d.get(rec_id)
rec2 = astra.data3d.get(rec_id2)
pylab.figure(1)
pylab.imshow(rec[50,:,:])
pylab.figure(2)
pylab.imshow(rec2[50,:,:])
np.save("refArr", rec2)
pylab.show()

astra.algorithm.delete(alg_id)
astra.data3d.delete(rec_id)
astra.data3d.delete(rec_id2)
astra.data3d.delete(proj_id)
